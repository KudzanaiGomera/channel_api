# README #

RESTFUL API with PHP

### What is this repository for? ###

created an api with php

### How do I get set up? ###

* clone the repo `git clone https://KudzanaiGomera@bitbucket.org/KudzanaiGomera/katanga_api.git`

* Copy the api folder into your htdocs folder and start your server

### How to test

* in your browser type the following link to return json data of the list of store Abrevations(Before checkin)

* `http://localhost/api/katangat_nielsons_api.php`


* in your browser type the following link and pass an abrevation in the quotes eg `http://localhost/api/katangat_nielsons_api.php?channelAbvr="PNP"`

* to return json data of the list of store Abrevations(After checkin)

* `http://localhost/api/katangat_nielsons_api.php?channelAbvr=""`


* How to run on live Server

depends


* Also posible to test with post man using a GET Method

### Contribution guidelines ###

* Writing tests -> Kudzanai
* Code review -> Junior and Sipho

### Who do I talk to? ###

* Repo owner or admin -> kudzanai
* Other community -> Katanga dev team