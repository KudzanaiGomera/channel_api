<?php

// displaying errors faced
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("./includes/db.inc.php");

//  After check in

//Get channelAbvr from VARS

$channelAbvr  = isset($_REQUEST['channelAbvr']) ? $_REQUEST['channelAbvr'] : null;

//response
$response = array();

// selecting the data from the table ref_channel
$sql_query = "SELECT channelCode, channelAbvr FROM ref_channel WHERE channelAbvr = $channelAbvr";

$sql_data = mysqli_query($con, $sql_query);

echo $channelAbvr;

// fetching data as an associative array
if($sql_data){
    $i = 0; 
    while($sql_data_err = mysqli_fetch_assoc($sql_data)){
        $response[$i]['channelCode'] = $sql_data_err['channelCode'];
        $response[$i]['channelAbvr'] = $sql_data_err['channelAbvr'];
        $i++;
    };

    // OUTPUT DATA (JSON)
    header("Content-Type: application/json");
    echo json_encode($response, JSON_PRETTY_PRINT);
}

//before check in
else{
    // selecting the data from the table ref_channel
    $sql_query = "SELECT channelAbvr FROM ref_channel";
    $sql_data = mysqli_query($con, $sql_query);

    // fetching data as an associative array
    if($sql_data){
        $i = 0; 
        while($sql_data_err = mysqli_fetch_assoc($sql_data)){
            $response[$i]['channelAbvr'] = $sql_data_err['channelAbvr'];
            $i++;
        };

        // OUTPUT DATA (JSON)
        header("Content-Type: application/json");
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
}
?>